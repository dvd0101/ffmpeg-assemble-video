#include "app.hpp"
#include <iostream>

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
}

app::av_codec_context configure_codec() {
    /*
     * Reference page with profiles/levels allowed for the H264 codec.
     * https://en.wikipedia.org/wiki/Advanced_Video_Coding#Profiles
     * https://en.wikipedia.org/wiki/Advanced_Video_Coding#Feature_support_in_particular_profiles
     * https://en.wikipedia.org/wiki/Advanced_Video_Coding#Levels
     *
     */
    const AVCodec* codec = avcodec_find_encoder_by_name("libx264");
    if (codec == nullptr)
        throw std::runtime_error("codec libx264 not found");

    // The max resolution and fps depends on the H264 level
    const int frame_width = 720;
    const int frame_height = 576;
    const int fps = 25;

    app::av_codec_context ctx(codec);

    // the pixel format depends on the H264 profile, for "baseline" is YUV 4:2:0
    ctx->pix_fmt = AV_PIX_FMT_YUV420P;
    ctx->width = frame_width;
    ctx->height = frame_height;
    ctx->time_base = AVRational{1, fps};
    // XXX gop_size controls how many frames before an I-frame is emitted
    // ctx->gop_size = 5;
    // XXX max_b_frames controls the max number of consecutive B-Frame
    // ctx->max_b_frames = 5;

    av_opt_set(ctx->priv_data, "profile", "baseline", 0);
    av_opt_set(ctx->priv_data, "level", "3", 0);
    // http://dev.beandog.org/x264_preset_reference.html has a nice table with
    // the presets recognised by libx264
    av_opt_set(ctx->priv_data, "preset", "fast", 0);

    // the AV_CODEC_FLAG_GLOBAL_HEADER is required when the *H264* codec is
    // used with *some* containers like *MKV*
    //
    // see: https://stackoverflow.com/a/60282127/981321
    //
    // XXX refactor this code so that the output format context is intancede
    // here like the codec contex; with the output context we can write:
    // ```
    // // Some formats want stream headers to be separate.
    // if (format_ctx->flags & AVFMT_GLOBALHEADER)
    //    ctx->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
    // ```
    ctx->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;

    if (int ret = avcodec_open2(ctx, codec, nullptr); ret < 0)
        throw app::ff_error("cannot open codec", ret);

    return ctx;
}

/**
 * print all the available formats (containers).
 *
 * also: `$ ffmpeg -formats`
 */
void print_all_formats() {
    void* i = nullptr;
    const AVOutputFormat* fmt = nullptr;
    while ((fmt = av_muxer_iterate(&i))) {
        std::cerr << fmt->name << " " << fmt->long_name << "\n";
        if (fmt->mime_type != nullptr || fmt->extensions != nullptr) {
            std::cerr << "\t";
            if (fmt->mime_type != nullptr)
                std::cerr << fmt->mime_type << " ";
            if (fmt->extensions != nullptr)
                std::cerr << fmt->extensions << " ";
            std::cerr << "\n";
        }
    }
}

/**
 * print all the available codecs.
 *
 * also:
 * - `$ ffmpeg -codecs`
 * - `$ ffmpeg -encoders`
 * - `$ ffmpeg -decoders`
 * - `$ ffmpeg -h encoder=h264`
 * - `$ ffmpeg -h decoder=h264`
 */
void print_all_codecs() {
    const AVCodecDescriptor* codec = nullptr;
    while ((codec = avcodec_descriptor_next(codec))) {
        std::cout << codec->name << " " << codec->long_name << "\n";
        if (codec->profiles != nullptr) {
            std::cout << "\tprofiles: ";
            for (int i = 0; codec->profiles[i].profile != FF_PROFILE_UNKNOWN; i++) {
                std::cout << "(" << codec->profiles[i].name << ") ";
            }
            std::cout << "\n";
        }
    }
}

int main(int argc, char* argv[]) {
    if (argc < 3) {
        std::cerr << "usage: " << argv[0] << " FRAMES_DIR INTERVAL [COUNT]\n";
        return 1;
    }

    std::string frames_dir = argv[1];
    int frame_interval = std::atoi(argv[2]);
    if (frame_interval <= 0) {
        std::cerr << "invalid frame interval: " << frame_interval << "\n";
        return 1;
    }

    int frames_count = 240;
    if (argc == 4) {
        frames_count = std::atoi(argv[3]);
        if (frames_count <= 0) {
            std::cerr << "invalid frames count: " << frames_count << "\n";
            return 1;
        }
    }

    auto codec_ctx = configure_codec();

    app::image_queue images(10);
    app::packet_queue encoded_packets;
    auto input_done = app::fetch_images(frames_dir, frames_count, frame_interval, images);
    auto encode_done = app::encode_frames(codec_ctx, images, encoded_packets);
    /* auto stream_done = app::stream_packets_to_mkv(codec_ctx, "test.mkv", encoded_packets); */
    auto stream_done = app::stream_packets_to_rtsp(codec_ctx, "rtsp://127.0.0.1:8554/mystream", encoded_packets);

    try {
        input_done.get();
    } catch (const std::exception& e) {
        std::cerr << "fetching thread exited with an error: " << e.what() << "\n";
    }

    try {
        encode_done.get();
    } catch (const std::exception& e) {
        std::cerr << "encoding thread exited with an error: " << e.what() << "\n";
    }

    try {
        stream_done.get();
    } catch (const std::exception& e) {
        std::cerr << "streaming thread exited with an error: " << e.what() << "\n";
    }

    std::cout << "exit\n";
}
