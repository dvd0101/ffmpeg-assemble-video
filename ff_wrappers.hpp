#pragma once

#include <memory>
#include <stdexcept>

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/opt.h>
#include <libswscale/swscale.h>
}

namespace app {
    class ff_error : public std::runtime_error {
      public:
        ff_error(const char* msg, int ret) : std::runtime_error(std::string(msg) + " " + av_make_error_string(ret)) {}

        /**
         * Converts a libav error to a string for human consumption.
         *
         * This method replaces the C macro `av_err2str` since it cannot be used with C++.
         */
        static std::string av_make_error_string(int errnum) {
            char errbuf[AV_ERROR_MAX_STRING_SIZE];
            av_strerror(errnum, errbuf, AV_ERROR_MAX_STRING_SIZE);
            return errbuf;
        }
    };

    class av_codec_context {
      public:
        av_codec_context(const AVCodec* codec) : ctx(nullptr, deallocate) {
            AVCodecContext* c = avcodec_alloc_context3(codec);
            if (c == nullptr)
                throw std::runtime_error("cannot allocate the context");
            ctx.reset(c);
        }

        operator AVCodecContext*() { return ctx.get(); }
        operator AVCodecContext*() const { return ctx.get(); }

        AVCodecContext* operator->() { return ctx.get(); }
        AVCodecContext* operator->() const { return ctx.get(); }

      private:
        static void deallocate(AVCodecContext* ctx) { avcodec_free_context(&ctx); }

        std::unique_ptr<AVCodecContext, void (*)(AVCodecContext*)> ctx;
    };

    class av_frame {
      public:
        av_frame() : ptr(nullptr, deallocate) {
            AVFrame* f = av_frame_alloc();
            if (f == nullptr)
                throw std::runtime_error("cannot allocate a frame");
            ptr.reset(f);
        }

        operator AVFrame*() { return ptr.get(); }

        AVFrame* operator->() { return ptr.get(); }

      private:
        static void deallocate(AVFrame* frame) { av_frame_free(&frame); }

        std::unique_ptr<AVFrame, void (*)(AVFrame*)> ptr;
    };

    class av_packet {
      public:
        av_packet() : ptr(nullptr, deallocate) {
            AVPacket* f = av_packet_alloc();
            if (f == nullptr)
                throw std::runtime_error("cannot allocate a packet");
            ptr.reset(f);
        }

        operator AVPacket*() { return ptr.get(); }

        AVPacket* operator->() { return ptr.get(); }

      private:
        static void deallocate(AVPacket* pkt) { av_packet_free(&pkt); }

        std::unique_ptr<AVPacket, void (*)(AVPacket*)> ptr;
    };

    class av_format_context {
      public:
        av_format_context(const char* format, const char* url) : ptr(nullptr, deallocate) {
            AVFormatContext* f;
            if (int ret = avformat_alloc_output_context2(&f, nullptr, format, url); ret < 0)
                throw ff_error("cannot allocate a format context", ret);
            ptr.reset(f);
        }

        av_format_context(const std::string& format, const std::string& url) : av_format_context(format.data(), url.data()) {}

        operator AVFormatContext*() { return ptr.get(); }
        operator AVFormatContext*() const { return ptr.get(); }

        AVFormatContext* operator->() { return ptr.get(); }
        AVFormatContext* operator->() const { return ptr.get(); }

      private:
        static void deallocate(AVFormatContext* ctx) { avformat_free_context(ctx); }

        std::unique_ptr<AVFormatContext, void (*)(AVFormatContext*)> ptr;
    };

    class sws_context {
      public:
        sws_context(SwsContext* ctx = nullptr) : ptr(ctx, deallocate) {}

        void replace(SwsContext* ctx) {
            if (ctx == ptr.get())
                return;
            ptr.release();
            ptr.reset(ctx);
        }

        operator SwsContext*() { return ptr.get(); }

        SwsContext* operator->() { return ptr.get(); }

      private:
        static void deallocate(SwsContext* ctx) { sws_freeContext(ctx); }

        std::unique_ptr<SwsContext, void (*)(SwsContext*)> ptr;
    };
}
