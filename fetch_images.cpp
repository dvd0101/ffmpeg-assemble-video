#include "app.hpp"
#include "stb_image.h"
#include <chrono>
#include <iostream>
#include <regex>
#include <thread>

namespace app {
    namespace {
        struct file_frame {
            std::filesystem::path path;
            int timestamp;
        };

        std::vector<file_frame> collect_frames(std::filesystem::path root, size_t count) {
            std::vector<file_frame> output;

            std::regex r("-(\\d+)");
            std::smatch match;
            for (auto& entry : std::filesystem::directory_iterator(root)) {
                std::string fname = entry.path().stem();
                if (std::regex_search(fname, match, r)) {
                    output.push_back({
                        entry.path(),
                        std::atoi(match.str(1).data()),
                    });
                }
            }

            std::sort(output.begin(), output.end(), [](auto& a, auto& b) { return a.timestamp < b.timestamp; });
            if (output.size() > count)
                output.resize(count);
            return output;
        }
    }

    void rgb_frame::deallocate(unsigned char* img) { stbi_image_free(img); }

    std::future<void> fetch_images(std::filesystem::path root, size_t count, int ts_step, image_queue& output) {
        std::promise<void> p;
        std::future<void> f = p.get_future();

        std::thread t(
            [=, &output](std::promise<void> p) {
                using namespace std::chrono_literals;

                p.set_value_at_thread_exit();
                for (const file_frame& image : collect_frames(root, count)) {
                    int ts = (image.timestamp - 1) * ts_step;
                    std::cout << "> [" << ts << "s] " << image.path << "\n";

                    int width, height, channels_in_file;
                    unsigned char* pixels = stbi_load(image.path.c_str(), &width, &height, &channels_in_file, 3);
                    if (pixels == nullptr) {
                        std::cout << ">! error opening file: " << stbi_failure_reason() << "\n";
                        continue;
                    }

                    rgb_frame frame(width, height, pixels, ts);
                    while (!output.try_enqueue(std::move(frame))) {
                        std::cout << ">>> image queue is full, throttling\n";
                        std::this_thread::sleep_for(1s);
                    }
                }
                output.enqueue(std::nullopt);
            },
            std::move(p));
        t.detach();

        return f;
    }
}
