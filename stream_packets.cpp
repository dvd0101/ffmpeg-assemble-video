#include "app.hpp"
#include <chrono>
#include <iostream>
#include <thread>

namespace app {
    namespace {
        void stream_loop(const av_format_context& format_ctx,
                         const av_codec_context& codec_ctx,
                         const std::string& url,
                         packet_queue& input) {
            std::cout << "» streaming " << format_ctx->oformat->name << " " << format_ctx->oformat->long_name << "\n";

            AVStream* st = avformat_new_stream(format_ctx, nullptr);
            if (st == nullptr)
                throw std::runtime_error("cannot create output stream");

            st->id = format_ctx->nb_streams - 1;
            st->time_base = codec_ctx->time_base;

            bool is_rtsp = strcmp(format_ctx->oformat->name, "rtsp") == 0;

            // XXX this should be:
            // if (!(format_ctx->flags & AVFMT_NOFILE)) {
            if (!is_rtsp) {
                // some protocols (like rtsp) does not use a AVIOContext, but the url stored in the ctx instead.
                if (int ret = avio_open(&format_ctx->pb, url.data(), AVIO_FLAG_WRITE); ret < 0)
                    throw ff_error("cannot open output file", ret);
            }

            avcodec_parameters_from_context(st->codecpar, codec_ctx);
            if (int ret = avformat_write_header(format_ctx, nullptr); ret < 0)
                throw ff_error("cannot write output headers", ret);

            using clock = std::chrono::high_resolution_clock;

            auto last_write = clock::now();
            int last_pts = 0;
            std::optional<av_packet> packet;
            while (true) {
                input.wait_dequeue(packet);
                if (!packet)
                    break;

                int codec_pts = (*packet)->pts;

                // even if the stream was configured with the same time_base of
                // the codec, it's value can change after copying the codec
                // parameters, so a rescale is necessary.
                //
                // see: https://stackoverflow.com/a/55348854/981321
                std::cout << "» writing packet pts:" << codec_pts;
                av_packet_rescale_ts(*packet, codec_ctx->time_base, st->time_base);
                std::cout << " -> " << (*packet)->pts << "\n";

                if (is_rtsp) {
                    using namespace std::chrono;
                    double r = 1. * codec_ctx->time_base.num / codec_ctx->time_base.den;
                    // delay wrt the previous packet
                    double packet_offset = (codec_pts - last_pts) * r;
                    // time elapsed since the previous write
                    auto elapsed = duration_cast<milliseconds>(clock::now() - last_write);

                    auto wait_time = milliseconds(static_cast<int>(packet_offset * 1000)) - elapsed;
                    /* std::cout << "========================== sleep for " << wait_time.count() << "ms\n"; */
                    /* if (wait_time.count() > r) */
                    /*     std::this_thread::sleep_for(wait_time); */
                    std::this_thread::sleep_for(milliseconds(20));

                    last_pts = codec_pts;
                }

                if (int ret = av_interleaved_write_frame(format_ctx, *packet); ret < 0)
                    throw ff_error("cannot write frame", ret);

                if (is_rtsp)
                    last_write = clock::now();
            }

            av_write_trailer(format_ctx);

            if (!is_rtsp) {
                avio_closep(&format_ctx->pb);
            }
        }

        std::future<void> stream_packets_to(av_format_context format_ctx,
                                            const av_codec_context& codec_ctx,
                                            const std::string& url,
                                            packet_queue& input) {
            std::promise<void> p;
            std::future<void> f = p.get_future();

            std::thread t(
                [&, format_ctx = std::move(format_ctx), url](std::promise<void> p) {
                    std::cout << "» start streamer thread\n";
                    try {
                        stream_loop(format_ctx, codec_ctx, url, input);
                    } catch (...) {
                        p.set_exception(std::current_exception());
                        return;
                    }
                    std::cout << "» streaming complete\n";
                    p.set_value();
                },
                std::move(p));
            t.detach();

            return f;
        }
    }

    std::future<void> stream_packets_to_mkv(const av_codec_context& codec_ctx,
                                            std::string file_name,
                                            packet_queue& input) {
        av_format_context format_ctx("matroska", file_name);
        return stream_packets_to(std::move(format_ctx), codec_ctx, file_name, input);
    }

    std::future<void> stream_packets_to_rtsp(const av_codec_context& codec_ctx, std::string url, packet_queue& input) {
        av_format_context format_ctx("rtsp", url);
        if (int ret = av_opt_set(format_ctx->priv_data, "rtsp_transport", "tcp", 0); ret < 0)
            throw ff_error("cannot set an option", ret);
        if (int ret = av_opt_set(format_ctx->priv_data, "rtsp_flags", "prefer_tcp", 0); ret < 0)
            throw ff_error("cannot set an option", ret);
        return stream_packets_to(std::move(format_ctx), codec_ctx, url, input);
    }
}
