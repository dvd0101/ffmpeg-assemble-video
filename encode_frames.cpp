#include "app.hpp"
#include <iostream>
#include <thread>
#include <vector>

namespace app {
    namespace {
        class encoder {
          public:
            encoder(const av_codec_context& codec_ctx) : ctx_{&codec_ctx} {
                frame_->format = codec_ctx->pix_fmt;
                frame_->width = codec_ctx->width;
                frame_->height = codec_ctx->height;

                if (int ret = av_frame_get_buffer(frame_, 0); ret < 0)
                    throw ff_error("cannot allocate the frame buffers", ret);

                sws_ = sws_getContext(codec_ctx->width,
                                      codec_ctx->height,
                                      AV_PIX_FMT_RGB24,
                                      codec_ctx->width,
                                      codec_ctx->height,
                                      AV_PIX_FMT_YUV420P,
                                      0,
                                      0,
                                      0,
                                      0);
            }

            /** Send an image to the encoder and returns the ready packets */
            std::vector<av_packet> send_frame(const rgb_frame& img) {
                // av_frame_make_writable makes a copy of the underlying buffer
                // if it's referenced by more than one owner; in pur case this
                // should be a no-op since we are the only owner of this frame
                if (int ret = av_frame_make_writable(frame_); ret < 0)
                    throw ff_error("cannot make frame writable", ret);

                const av_codec_context& ctx = *ctx_;

                copy_img_to_frame(img);
                frame_->pts = img.timestamp() * ctx->time_base.den / ctx->time_base.num;

                if (int ret = avcodec_send_frame(ctx, frame_); ret < 0)
                    throw ff_error("cannot send the frame to the encoder", ret);

                return drain_codec();
            }

            /**
             * Flush the encoder state and returns the ready packets
             *
             * This function must be called at least once after the last image
             * was sent to the encoder.
             */
            std::vector<av_packet> flush() {
                if (int ret = avcodec_send_frame(*ctx_, nullptr); ret < 0)
                    throw ff_error("cannot send the frame to the encoder", ret);
                return drain_codec();
            }

          private:
            /** Copy an rgb image to the frame to be encoded, tacking care of the resize and format conversions */
            void copy_img_to_frame(const rgb_frame& img) {
                // we use replace() because sws_getCachedContext may deallocate
                // the old context and allocate a new one; if it deallocates
                // the old one our smart-pointer must forget it.
                sws_.replace(sws_getCachedContext(sws_,
                                                  img.width(),
                                                  img.height(),
                                                  AV_PIX_FMT_RGB24,
                                                  (*ctx_)->width,
                                                  (*ctx_)->height,
                                                  AV_PIX_FMT_YUV420P,
                                                  0,
                                                  0,
                                                  0,
                                                  0));
                // our frame is packed, there is not padding between rows
                const int src_stride[1] = {3 * img.width()};
                const unsigned char* src_pixels = img.pixels();
                sws_scale(sws_, &src_pixels, src_stride, 0, img.height(), frame_->data, frame_->linesize);
            }

            /** Extracts all the ready packets from the encoder */
            std::vector<av_packet> drain_codec() {
                std::vector<av_packet> pkts;

                while (true) {
                    av_packet pkt;
                    int ret = avcodec_receive_packet(*ctx_, pkt);
                    if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
                        break;
                    if (ret < 0)
                        throw ff_error("cannot extract packet from the encoder", ret);
                    std::cout << "| packet pts:" << pkt->pts << " encoded (" << pkt->size << " bytes)\n";
                    pkts.push_back(std::move(pkt));
                }
                return pkts;
            }

          private:
            // the codec context configured elsewhere
            const av_codec_context* ctx_;
            // a single frame used to encode all the images
            av_frame frame_;
            // the sw-scaler context used to resixe/convert the images
            sws_context sws_;
        };

        void encode_loop(const av_codec_context& codec_ctx, image_queue& input, packet_queue& output) {
            encoder enc(codec_ctx);

            auto send_packets = [&](auto packets) {
                for (av_packet& pkt : packets)
                    output.enqueue(std::move(pkt));
            };

            std::optional<rgb_frame> frame;
            while (true) {
                input.wait_dequeue(frame);
                if (!frame)
                    break;
                std::cout << "| frame received [" << frame->timestamp() << "] " << frame->width() << "x"
                          << frame->height() << "\n";
                send_packets(enc.send_frame(*frame));
            }
            send_packets(enc.flush());
        }

    }

    std::future<void> encode_frames(const av_codec_context& codec_ctx, image_queue& input, packet_queue& output) {
        std::promise<void> p;
        std::future<void> f = p.get_future();

        std::thread t(
            [&](std::promise<void> p) {
                std::cout << "| start encoding thread\n";
                try {
                    encode_loop(codec_ctx, input, output);
                    output.enqueue(std::nullopt);
                } catch (...) {
                    output.enqueue(std::nullopt);
                    p.set_exception(std::current_exception());
                    return;
                }
                std::cout << "| encoding complete\n";
                p.set_value();
            },
            std::move(p));
        t.detach();

        return f;
    }
}
