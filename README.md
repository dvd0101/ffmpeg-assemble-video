# ffmpeg-assemble-video

A POC project to test libavcodec + libavformat to assemble an H264 video from a
non continous stream of images and mux it into a MKV container or a RTSP stream
sent to a remote server.

# Compile and run

```
$ cmake -S . -B build
$ cmake --build build
$ ./build/proto_av FRAMES_DIR INTERVAL [COUNT]
```

## Frames directory

This programs read the frames to encode from a local directory; frames must be
named like: `img-XXX.jpeg` where *XXX* is the sequence number starting from
*000*.

Images are frames taken every INTERVAL seconds (for this prototype the interval
must be constant, but the code can be refactored to support non-constant
intervals).

To create the frames from a video file you can use ffmpeg, for example:
```
$ ffmpeg -i ./Five\ Nights\ at\ Freddy\'s\ Security\ Camera\ Footage-pB0lxdsGkBs.mkv -vsync cfr -r 1 'frames1/img-%03d.jpeg'
```

## Select MKV or RTSP

Comment/uncomment revelant lines in main.cpp and recompile
