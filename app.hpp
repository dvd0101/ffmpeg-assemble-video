#pragma once

#include "ff_wrappers.hpp"
#include "spsc/readerwriterqueue.h"
#include <filesystem>
#include <future>
#include <optional>

namespace app {
    /**
     * An input frame encoded as a RGB24 sequence of bytes.
     *
     * Every frame has a timestamp, the number of seconds since the start of
     * the image stream. XXX Probably a std::chrono::time_point is a better idea.
     */
    class rgb_frame {
      public:
        rgb_frame(int w, int h, unsigned char* px, int t)
            : width_{w}, height_{h}, pixels_{px, deallocate}, timestamp_{t} {}

        int width() const { return width_; }
        int height() const { return height_; }
        unsigned char* pixels() const { return pixels_.get(); };
        int timestamp() const { return timestamp_; }

      private:
        static void deallocate(unsigned char*);

        int width_ = 0;
        int height_ = 0;
        std::unique_ptr<unsigned char[], void (*)(unsigned char*)> pixels_;
        int timestamp_ = 0;
    };

    using moodycamel::BlockingReaderWriterQueue;
    using image_queue = BlockingReaderWriterQueue<std::optional<rgb_frame>>;
    using packet_queue = BlockingReaderWriterQueue<std::optional<av_packet>>;

    /**
     * Fetch thread; collects, decodes and push the images found on a local
     * path to the output queue.
     *
     * Images are converted to `rgb_frame`s and annotated with a timestamp (the
     * first image starts at 0).
     *
     * This thread auto-throttle itsell when the queue is full.
     *
     * @param root is the local directory from which the images are collected.
     * @param count is the number of images to collect
     * @param step is the number of seconds between images
     * @param output queue
     */
    std::future<void> fetch_images(std::filesystem::path root, size_t count, int ts_step, image_queue& output);

    /**
     * Encode thread; reads `rgb_frame`s from the input queue, encodes them
     * using the given codec and writes them to the output queue.
     *
     * @param codec_ctx is the codec to use
     * @param input queue
     * @param output queue
     */
    std::future<void> encode_frames(const av_codec_context& codec_ctx, image_queue& input, packet_queue& output);

    /**
     * Stream thread; reads packets from the input queue and mux them to a MKV
     * file
     *
     * @param codec_ctx is the codec used to encode the packets
     * @param file_name is the output name for the MKV file
     * @param input queue
     */
    std::future<void> stream_packets_to_mkv(const av_codec_context& codec_ctx,
                                            std::string file_name,
                                            packet_queue& input);

    /**
     * Stream thread; reads packets from the input queue and mux them to a RTSP
     * stream.
     *
     * @param codec_ctx is the codec used to encode the packets
     * @param url of the RTSP server
     * @param input queue
     */
    std::future<void> stream_packets_to_rtsp(const av_codec_context& codec_ctx, std::string url, packet_queue& input);
}
